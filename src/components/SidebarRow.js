import { Avatar } from '@material-ui/core';
import React from 'react'
import "../css/SidebarRow.css";


function SidebarRow ({ src, Icon, title}) {

    return (

        <div className="sidebarrow">
            {src && <Avatar src={src} /> }     {/* == (if src retourn avatar avec la source (image)) */}
            {Icon && <Icon />}                  {/* == (if Icon retourn Icon) */}
            <h4> {title} </h4>

        </div>

    );
}
export default SidebarRow;
