import React, {useState, useEffect} from 'react'
import "../css/Feed.css"
import StoryReel from "../components/StoryReel";
import MessageSender from "../components/MessageSender";
import Post from "../components/Post";
import db from '../firebase';
import {useStateValue} from '../provider/StateProvider';



function Feed() {

    const [ {user}, dispatch ] = useStateValue();
    
    const [ posts, setPosts] = useState([]);

    useEffect(() => {
        db.collection('posts')
            .orderBy('timestamp', 'desc')
            .onSnapshot( snapshot => (
            setPosts(snapshot.docs.map(doc => ({ id: doc.id, data:doc.data()}) ) )
        ))
       
    }, [])
 
    return (
        <div className="feed">            
            <StoryReel />
            <MessageSender /> 

            {posts.map((post) => (

                <>
                <Post 
                key={post.id}
                profilPic={post.data.profilPic}
                message={post.data.message}
                username={post.username}
                image={post.data.image}
                timestamp={post.data.timestamp}                
                />

                </>

            ))}
              
        </div>
    );
}
export default Feed;