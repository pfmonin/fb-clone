import React from 'react'
import "../css/Widget.css"

function Widget() {
    return (
        <div className="widget">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" className="fbIframe" width="340" height="100%" style={{ border:"none", overflow:"hidden"}} scrolling="no" frameBorder="0"  allow="encrypted-media" crossOrigin="true"></iframe>
        </div>     
    )
}

export default Widget
