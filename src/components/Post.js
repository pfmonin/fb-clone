import React from 'react'
import "../css/Post.css"
import { Avatar } from '@material-ui/core';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import CommentIcon from '@material-ui/icons/Comment';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import NearMeIcon from '@material-ui/icons/NearMe';


function Post({profilPic, image, username, timestamp, message}) {
    return (
        <div className="post">
            <div className="post__top">
            <Avatar src={profilPic} className="post__avatar" />
                <div className="post__topInfo">
                    <h3>{username}</h3>
                    <p> {new Date(timestamp?.toDate()).toUTCString()} </p>
                </div>
            </div>

            <div className="post__bottom">
                <p>{message}</p>
            </div>

            <div className="post__image">
                <img src={image} alt=""/>
            </div>

            <div className="post__options">
                <div className="post__option">
                    <ThumbUpIcon />
                    <p>Like</p>
                </div>

                <div className="post__option">
                    <CommentIcon />
                    <p>Comment</p>
                </div>

                <div className="post__option">
                    <NearMeIcon />
                    <p>Share</p>
                </div>


                <div className="post__option">
                  
                   <ExpandMoreIcon />
                </div>

            </div>
            
        </div>
    )
}

export default Post
